# SPDX-License-Identifier: Apache-2.0
# Copyright (C) 2020 The LineageOS Project

import common
import re

def AddImage(info, basename, dest):
  name = basename
  data = info.input_zip.read("IMAGES/" + basename)
  common.ZipWriteStr(info.output_zip, name, data)
  info.script.AppendExtra('package_extract_file("%s", "%s");' % (name, dest))

def PrintInfo(info, dest):
  info.script.Print("Patching {} image unconditionally...".format(dest.split('/')[-1]))

def OTA_InstallEnd(info):
  AddImage(info, "dtbo.img", "/dev/block/by-name/dtbo")
  AddImage(info, "vbmeta.img", "/dev/block/by-name/vbmeta")
  return

def FullOTA_InstallBegin(info):
  AddImage(info, "super_empty.img", "/dev/block/by-name/super")
  return

def FullOTA_Assertions(info):
  return

def IncrementalOTA_Assertions(info):
  return

def FullOTA_InstallEnd(info):
  OTA_InstallEnd(info)
  return

def IncrementalOTA_InstallEnd(info):
  OTA_InstallEnd(info)
  return
